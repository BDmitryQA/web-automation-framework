#for debug use docker exec -it <container name> /bin/bash

create_api_container: ## Starts APP container
	@echo "************* Building APP container *************"
	@docker-compose -p waf up -d --build api
	@echo "************* APP Container built *************"

create_at_container: ## Starts Auto Tests container
	@echo "************* Building Auto Test container *************"
	@docker-compose -p waf up -d --build auto_tests
	@echo "************* Auto Test Container built *************"

create_reports_container: ## Starts Reports container
	@echo "************* Building Reports container *************"
	@docker-compose -p waf up -d --build allure_reports
	@echo "************* Reports Container built *************"

stop_reports: ## Stops Reports container
	@docker stop allure_reports

create_containers: ## Starts AT and API containers
	@echo "************* Building containers *************"
	@make create_api_container
	@make create_at_container
	@echo "************* Containers built *************"

local_auto_tests_run: ## Run Auto Tests in auto_tests container locally
	@make stop_reports || echo "************* No Reports container found *************"
	@make create_reports_container
	@echo "************* Starting tests *************"
	@docker exec auto_tests /bin/bash -c "pytest -n2 --dist loadscope --max-worker-restart 0 --alluredir=/web-automation-framework/src/reports/allure-results/ /web-automation-framework/src/tests/; exit 0"
	@echo "************* Tests execution finished *************"
	@echo "************* Creating Report *************"
	@echo "Wait for report generation. Report Link - http://localhost:8081/"
	@docker exec allure_reports /bin/bash -c "allure generate --clean /web-automation-framework/src/reports/allure-results && python -m http.server -d allure-report 8081"

ci_auto_tests_run: ## Run Auto Tests on auto_tests container
	@echo "************* Starting tests *************"
	@docker exec auto_tests /bin/bash -c "pytest -n2 --dist loadscope --max-worker-restart 0 --alluredir=/web-automation-framework/src/reports/allure-results/ /web-automation-framework/src/tests/; exit 0"
	@echo "************* Tests execution finished *************"
	@echo "************* Copy reports and logs to the host *************"
	@docker cp auto_tests:/web-automation-framework/src/reports/allure-results ./src/reports/
	@docker cp auto_tests:/web-automation-framework/src/logs ./src/logs/
	@echo "************* Reports copied *************"

full_prune: ## Clean stopped containers, unused networks, unused volumes, images, and builder cache
	@docker-compose down -v --remove-orphans
	@docker system prune
	@docker builder prune -a
	@docker image prune -a
	@docker volume prune
