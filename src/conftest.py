# HTTP Logger through the seleniumwire


import os
import json
import pytest
import allure
from seleniumwire import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from src.global_configs import Configs
from src.testdata.td_generation import get_datetime


@pytest.fixture(scope="function")
def get_driver(request):
    driver = run_driver_instance()
    request.cls.driver = driver
    driver.implicitly_wait(Configs.short_delay)
    driver.get(Configs.url)
    yield
    driver.delete_all_cookies()
    driver.close()
    driver.quit()


def run_driver_instance():
    Configs.get()
    if Configs.browser == 'chrome':
        options = Options()
        options.add_argument(f'--window-size={Configs.screen_size}')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('--disable-infobars')
        options.add_argument('--disable-extensions')
        options.add_argument('--disable-browser-side-navigation')
        options.add_argument("--mute-audio")
        # options.add_argument('--auto-open-devtools-for-tabs')
        # options.add_argument(f'user-data-dir={Configs.user_dir}')
        # options.add_argument('--disable-features=ChromeWhatsNewUI')
        # options.page_load_strategy = 'normal'
        if Configs.mode == 'headless':
            options.add_argument('--headless')
            options.add_argument('--disable-gpu')
        elif Configs.mode != 'headless' and Configs.mode != 'normal':
            raise Exception(f'or "{Configs.mode}" is not supported run mode')
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        options.add_experimental_option('useAutomationExtension', False)
        options.add_experimental_option('prefs', {"plugins.always_open_pdf_externally": True})
        cap = DesiredCapabilities.CHROME
        cap['goog:loggingPrefs'] = {'browser': 'SEVERE'}
        # Available log levels:
        # -ALL
        # -DEBUG
        # -INFO
        # -SEVERE
        # -WARNING
        s_options = {'enable_har': False}
        executable_path = Configs.driver_path
        driver = webdriver.Chrome(options=options, executable_path=executable_path, desired_capabilities=cap,
                                  seleniumwire_options=s_options)
        params = {'behavior': 'allow', 'downloadPath': Configs.download_dir}
        driver.execute_cdp_cmd('Page.setDownloadBehavior', params)
        return driver
    elif Configs.browser != 'chrome':
        raise Exception(f'"{Configs.browser}" is not a supported browser ')


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item):
    outcome = yield
    rep = outcome.get_result()
    marker = item.get_closest_marker("ui")
    log_name = item.name + '_' + str(get_datetime()) + '_network_log.txt'
    src_dir = os.path.dirname(__file__)
    logs_dir = os.path.join(src_dir, 'logs')
    log_status_path = os.path.join(logs_dir, 'status')
    if not os.path.isdir(logs_dir):
        os.mkdir(logs_dir)
    log_path = os.path.join(src_dir, 'logs', log_name)
    if marker:
        if rep.when == "call" and rep.failed:
            with open(log_status_path, 'w') as f:
                f.write("failed")
            try:
                get_network_log(item.instance.driver, log_path)
                if os.path.exists(log_path):
                    allure.attach.file(log_path,
                                       name=item.name + '_network_logs',
                                       attachment_type=allure.attachment_type.TEXT)
                logs_b = item.instance.driver.get_log('browser')
                logs_browser = '\n'.join([i['message'] for i in logs_b])
                allure.attach(logs_browser,
                              name=item.name + '_browser_console',
                              attachment_type=allure.attachment_type.TEXT)
                page_source = str(item.instance.driver.page_source)
                logs_page_source = page_source[page_source.find('<body'):]
                allure.attach(logs_page_source,
                              name=item.name + '_page_source_logs',
                              attachment_type=allure.attachment_type.TEXT)
                allure.attach(item.instance.driver.get_screenshot_as_png(),
                              name=item.name,
                              attachment_type=allure.attachment_type.PNG)
            except Exception as e:
                print(e)


def get_network_log(driver, log_path):
    print('Test Failed. Get network logs')
    for request in driver.requests:
        date = str(request.date)
        blacklisted = bool([url for url in Configs.blacklist if url in request.url])
        if not blacklisted:
            print(f'{request.url} Check response')
            if request.response:
                if request.response.headers['content-type']:
                    if request.response.headers['content-type'] == "application/json":
                        request_body = None
                        response_body = None
                        if request.body.decode("utf-8") != '':
                            request_body = json.loads(request.body)
                        else:
                            print(f'{request.url} Empty request body')
                        if request.response.body.decode("utf-8") != '':
                            response_body = json.loads(request.response.body)
                            print(f'{request.url} Empty response body')
                        network = {
                            'date': date,
                            'request':
                                {'request_url': request.url,
                                 'method': request.method,
                                 'body': request_body,
                                 'response':
                                     {'status_code': request.response.status_code,
                                      'response_body': response_body}
                                 }
                        }
                        json_network = json.dumps(network, indent=4)
                        with open(log_path, 'a') as f:
                            f.write(json_network)
                        print(f'{request.url} Request added to network log')
                    else:
                        print(f'{request.url} Incorrect type of content-type. "application/json" expected')
                else:
                    print(f'{request.url} No content-type in header')
            else:
                print(f'{request.url} No response')
