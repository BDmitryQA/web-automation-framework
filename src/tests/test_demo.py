import pytest
import allure
from flaky import flaky
from random import randint
from src.pages.main.main_page import MainPageHelper


@pytest.mark.usefixtures("get_driver")
class TestSuite:

    @allure.title("Demonstration of the Failed test")
    @pytest.mark.ui
    def test_failed(self):
        # Open Website
        page = MainPageHelper(self.driver)
        page.is_opened()

        # Verification
        page.verify_failed()

    @allure.title("Demonstration of the Broken test")
    @pytest.mark.ui
    def test_broken(self):
        # Open Website
        page = MainPageHelper(self.driver)
        page.is_opened()

        # Click button
        page.click_on_non_existent_button()

    @allure.title("Demonstration of the Skipped test")
    @pytest.mark.ui
    @pytest.mark.xfail(reason="Skipped")
    def test_skipped(self):
        # Open Website
        main_page = MainPageHelper(self.driver)
        main_page.is_opened()

        # Verification
        main_page.verify_failed()

    @allure.title("Demonstration of the Flaky test")
    @pytest.mark.ui
    @flaky(max_runs=4, min_passes=2)
    def test_flaky(self):
        # Open Website
        page = MainPageHelper(self.driver)
        page.is_opened()

        # Verification
        number = randint(0, 1000)
        if (number % 2) == 0:
            page.verify_pass()
        else:
            page.verify_failed()
