import pytest
import allure
from scripts.crash_logger import add_to_logger
from src.pages.main.main_page import MainPageHelper


@pytest.mark.usefixtures("get_driver")
class TestSuite:

    @allure.title("Test Website link")
    @allure.link('http://qa-guide.net/', name='Click me')
    @pytest.mark.ui
    def test_website_link(self):
        # Test Data
        parameter1 = 'Test1'
        parameter2 = 'Test2'
        parameter3 = 'Test3'
        link = "http://qa-guide.net/"

        # Open Website
        page = MainPageHelper(self.driver)
        page.is_opened()
        page.add_screenshot('Site Screenshot')

        # Other Steps
        page.some_other_step_nested()
        page.some_other_step_parameter(parameter1, parameter2, parameter3)
        page.some_other_step_parameters(parameter1, parameter2)

        # Verification
        add_to_logger(driver=self.driver, action=f'verify_website_link')
        page.verify_website_link(link)
