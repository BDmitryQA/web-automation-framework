import re
import random
import socket
import string
from datetime import datetime, timedelta


def get_free_port():
    while True:
        port = random.randint(32768, 61000)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if not (sock.connect_ex(('127.0.0.1', port)) == 0):
            return port


def generate_phone():
    first = str(random.randint(100, 999))
    second = str(datetime.now().timestamp())[-3:]
    last = str(datetime.now().timestamp())[-4:]
    phone = '(' + first + ') ' + second + '-' + last
    return phone


def generate_email(email):
    split = re.split(r'@', email)
    time = get_datetime()
    generated_email = split[0] + '+' + str(time) + '@' + split[1]
    return generated_email


def get_datetime():
    current_time = datetime.utcnow()
    return current_time.strftime('%m%d%H%M%S')


def generate_random_data(N):
    data = ''.join(random.choices(string.hexdigits + string.punctuation, k=N))
    return data
