import os
import configparser
from src.testdata.td_generation import get_datetime, get_free_port


class Configs:
    # Screen configs
    mode = None
    screen_size = None

    # URL configs
    url = None
    cookie_domain = None

    # API configs
    api = None
    api_ver = None

    # Browser configs
    browser = None
    download_dir = None
    user_dir = None
    driver_path = None

    # HTTP Logger
    blacklist = None
    network_log = False
    network_type_request = False
    network_type_response = False

    # Wait configs
    delay = None
    long_delay = None
    short_delay = None

    # Email configs
    imap_address = None
    imap_port = None
    smtp_address = None
    smtp_port_ssl = None
    smtp_port_tls = None
    temp_email_service = None
    temp_email_api = None

    # Internal configs
    test_name = None
    configs_time = None
    process_pid = None
    log_dir = None
    tmp_log_path = None
    free_port = None

    @classmethod
    def get(cls):
        # Configs Read
        config = configparser.ConfigParser()
        config_file = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, 'CONFIG.ini'))
        config.read(config_file)
        config.read('CONFIG.ini')

        # Screen configs
        Configs.mode = config['Screen_settings']['mode']
        Configs.screen_size = config['Screen_settings']['screen_size']

        # URL configs
        Configs.url = os.environ.get('WEBAPP_URL', 'http://localhost:3000/app')
        Configs.cookie_domain = os.environ.get('WEBAPP_URL', 'localhost')

        # API configs
        Configs.api = Configs.api = Configs.url
        Configs.api_ver = config['API_settings']['api_ver']

        # Browser configs
        Configs.browser = config['Browser_settings']['browser']
        Configs.download_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, 'downloads'))
        Configs.user_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, 'chrome_user'))
        driver_path = os.path.join(os.path.dirname(__file__), 'driver_path.txt')
        with open(driver_path, 'r') as f:
            Configs.driver_path = f.read()

        # HTTP Logger
        Configs.blacklist = config['HTTP_logger']['blacklist'].split(', ')
        Configs.network_log = str_to_bool(config['HTTP_logger']['network_log'])
        Configs.network_type_request = str_to_bool(config['HTTP_logger']['network_type_request'])
        Configs.network_type_response = str_to_bool(config['HTTP_logger']['network_type_response'])

        # Wait configs
        Configs.delay = int(config['Wait_configs']['delay'])
        Configs.long_delay = int(config['Wait_configs']['long_delay'])
        Configs.short_delay = int(config['Wait_configs']['short_delay'])

        # Email configs
        Configs.imap_address = config['Email_configs']['imap_address']
        Configs.imap_port = config['Email_configs']['imap_port']
        Configs.smtp_address = config['Email_configs']['smtp_address']
        Configs.smtp_port_ssl = config['Email_configs']['smtp_port_ssl']
        Configs.smtp_port_tls = config['Email_configs']['smtp_port_tls']
        Configs.temp_email_service = config['Email_configs']['temp_email_service']
        Configs.temp_email_api = config['Email_configs']['temp_email_api']

        # Internal configs
        Configs.configs_time = get_datetime()
        Configs.process_pid = os.getpid()
        src_dir = os.path.dirname(__file__)
        log_name = f'{Configs.configs_time}_' + '{}' + f'_log_{Configs.test_name}.txt'
        Configs.log_dir = os.path.join(src_dir, 'logs')
        Configs.tmp_log_path = os.path.join(Configs.log_dir, log_name)
        Configs.free_port = get_free_port()


def str_to_bool(string):
    return string.lower() in ("yes", "True", "true", "t", "1")
