# Alternative HTTP Logger through the pychrome

import json
import os
import pychrome
import pytest
import allure
from pychrome import CallMethodException
from selenium import webdriver
from functools import partial
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.chrome.service import Service as ChromeService
from src.global_configs import Configs


@pytest.fixture(scope="function")
def get_driver(request):
    Configs.test_name = request.keywords.node.originalname
    driver = run_driver_instance()
    request.cls.driver = driver
    if Configs.network_log:
        tab = connect_to_browser_dev_tools()
        if Configs.network_type_request:
            tab.set_listener('Network.requestWillBeSent', request_basic_data)
        if Configs.network_type_response:
            tab.set_listener('Network.responseReceived', partial(response_basic_data, tab))
    yield
    driver.delete_all_cookies()
    driver.close()
    driver.quit()


def run_driver_instance():
    Configs.get()
    if Configs.browser == 'chrome':
        options = ChromeOptions()
        options.add_argument(f'--window-size={Configs.screen_size}')
        options.add_argument('--incognito')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('--disable-infobars')
        options.add_argument('--disable-extensions')
        options.add_argument('--disable-browser-side-navigation')
        options.add_argument('--mute-audio')
        # options.add_argument('--auto-open-devtools-for-tabs')
        # options.add_argument(f'user-data-dir={Configs.user_dir}')
        # options.add_argument('--disable-features=ChromeWhatsNewUI')
        # options.page_load_strategy = 'normal'
        if Configs.network_log:
            options.add_argument(f'--remote-debugging-port={Configs.free_port}')
        if Configs.mode == 'headless':
            options.add_argument('--headless')
            options.add_argument('--disable-gpu')
        elif Configs.mode != 'headless' and Configs.mode != 'normal':
            raise Exception(f'or "{Configs.mode}" is not supported run mode')
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        options.add_experimental_option('useAutomationExtension', False)
        options.add_experimental_option('prefs', {"plugins.always_open_pdf_externally": True})
        logging_prefs = {'browser': 'WARNING'}
        # Available log levels:
        # -ALL
        # -DEBUG
        # -INFO
        # -SEVERE
        # -WARNING
        options.set_capability('goog:loggingPrefs', logging_prefs)
        executable_path = Configs.driver_path
        service = ChromeService(executable_path=executable_path)
        driver = webdriver.Chrome(options=options, service=service)
        params = {'behavior': 'allow', 'downloadPath': Configs.download_dir}
        driver.execute_cdp_cmd('Page.setDownloadBehavior', params)
        return driver
    elif Configs.browser != 'chrome':
        raise Exception(f'"{Configs.browser}" is not a supported browser ')


def connect_to_browser_dev_tools():
    dev_tools = pychrome.Browser(url=f'http://localhost:{Configs.free_port}')
    tab = dev_tools.list_tab()[0]
    tab.start()
    tab.call_method('Network.enable', _timeout=20)
    return tab


def request_basic_data(**kwargs):
    req_tmp_log_path = Configs.tmp_log_path.format('request')
    request_data = kwargs
    if 'request' in request_data:
        request = request_data['request']
        request_url = request['url']
        if Configs.url in request_url \
                and '.js' not in request_url \
                and '.css' not in request_url \
                and '.png' not in request_url:
            formatted_network_log = json.dumps(request_data, indent=4)
            with open(req_tmp_log_path, 'a') as f:
                f.write(formatted_network_log)


def response_basic_data(tab, **kwargs):
    resp_tmp_log_path = Configs.tmp_log_path.format('response')
    response_data = kwargs
    response_id = kwargs.get('requestId')
    response_date = 'None'
    response_content_type = 'None'
    response_body = {'body': 'None'}
    if 'response' in response_data:
        response = response_data['response']
        if Configs.api in response['url']:
            try:
                if response['headers']:
                    response_headers = response['headers']
                    response_date = response_headers['date']
                    response_content_type = response_headers['content-type']
                    response_body = tab.call_method('Network.getResponseBody', requestId=response_id)
            except CallMethodException:
                print('CallMethodException')
            network_log = {
                'date': response_date,
                'response_id': response_id,
                'response':
                    {'response_url': response['url'],
                     'status': response['status'],
                     'statusText': response['statusText'],
                     'Content-Type': response_content_type,
                     'response_body': response_body['body']
                     }
            }
            formatted_network_log = json.dumps(network_log, indent=4)
            with open(resp_tmp_log_path, 'a') as f:
                f.write(formatted_network_log)


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item):
    outcome = yield
    rep = outcome.get_result()
    tmp_log_path = Configs.tmp_log_path
    logs_dir = Configs.log_dir
    log_status_path = os.path.join(logs_dir, 'status')
    req_tmp_log_path = tmp_log_path.format('request')
    resp_tmp_log_path = tmp_log_path.format('response')
    marker = item.get_closest_marker("ui")
    if marker:
        if rep.when == "call" and rep.passed:
            if os.path.exists(req_tmp_log_path):
                os.remove(req_tmp_log_path)
            if os.path.exists(resp_tmp_log_path):
                os.remove(resp_tmp_log_path)
        if rep.when == "call" and rep.failed:
            with open(log_status_path, 'w') as f:
                f.write("failed")
            try:
                if os.path.exists(req_tmp_log_path):
                    allure.attach.file(req_tmp_log_path,
                                       name=item.name + '_req_network_log',
                                       attachment_type=allure.attachment_type.TEXT)
                if os.path.exists(resp_tmp_log_path):
                    allure.attach.file(resp_tmp_log_path,
                                       name=item.name + '_resp_network_log',
                                       attachment_type=allure.attachment_type.TEXT)
                logs_b = item.instance.driver.get_log('browser')
                logs_browser = '\n'.join([i['message'] for i in logs_b])
                allure.attach(logs_browser,
                              name=item.name + '_browser_console',
                              attachment_type=allure.attachment_type.TEXT)
                page_source = str(item.instance.driver.page_source)
                logs_page_source = page_source[page_source.find('<body class="'):]
                allure.attach(logs_page_source,
                              name=item.name + '_page_source_logs',
                              attachment_type=allure.attachment_type.TEXT)
                allure.attach(item.instance.driver.get_screenshot_as_png(),
                              name=item.name,
                              attachment_type=allure.attachment_type.PNG)
            except Exception as e:
                print(e)
