import allure
import os
import time
from src.global_configs import Configs
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import WebDriverException, TimeoutException
from src.api.api_requests import get_user_data_from_login


class Locators:
    PAGE_LOADER = "//*[@id='page-loader']"
    PRELOADER = "//*[@id='preloader']"
    RENDER_PRELOADER = "//*[@id='render-preloader']"


class BasePage(object):

    def __init__(self, driver):
        self.driver = driver

    ################################################# wait_functions ###################################################

    def wait_one_of_elements_presence(self, locator, element_number=0, short_delay=False, long_delay=False):
        elements = self.wait_all_elements_presence(locator, short_delay, long_delay)
        one_of_elements = elements[element_number]
        return one_of_elements

    def wait_all_elements_presence(self, locator, short_delay=False, long_delay=False):
        wait = self.delay(short_delay, long_delay)
        elements = wait.until(ec.presence_of_all_elements_located(locator),
                              f"Can't find elements by locator {locator}")
        return elements

    def wait_element_clickable(self, locator, short_delay=False, long_delay=False):
        self.wait_full_upload()
        wait = self.delay(short_delay, long_delay)
        element = wait.until(ec.element_to_be_clickable(locator),
                             f"Can't find element by locator {locator}")
        return element

    def wait_element_invisibility(self, locator, short_delay=False, long_delay=False):
        self.wait_full_upload()
        wait = self.delay(short_delay, long_delay)
        return wait.until(ec.invisibility_of_element_located(locator),
                          f"Can't find element by locator {locator}")

    def wait_one_of_elements_visibility(self, locator, element_number=0, short_delay=False, long_delay=False):
        elements = self.wait_all_elements_visibility(locator, short_delay, long_delay)
        one_of_elements = elements[element_number]
        return one_of_elements

    def wait_all_elements_visibility(self, locator, short_delay=False, long_delay=False):
        self.wait_full_upload()
        wait = self.delay(short_delay, long_delay)
        elements = wait.until(ec.visibility_of_all_elements_located(locator),
                              f"Can't find elements by locator {locator}")
        return elements

    def wait_one_of_elements_attribute_loaded(self, locator, attribute='value', element_number=0):
        self.wait_full_upload()
        wait = WebDriverWait(self.driver, Configs.delay)
        one_of_elements = self.wait_one_of_elements_presence(locator, element_number)
        wait.until(lambda driver: one_of_elements.get_attribute(attribute) != ''),
        f"Attribute {attribute} of one from elements with locator {locator}, not loaded"
        return one_of_elements

    def wait_all_elements_attributes_loaded(self, locator, attribute='value'):
        self.wait_full_upload()
        wait = WebDriverWait(self.driver, Configs.delay)
        elements = self.wait_all_elements_presence(locator)
        for element in elements:
            wait.until(lambda driver: element.get_attribute(attribute) != ''),
            f"Attribute {attribute} of one from elements with locator {locator}, not loaded"
        return elements

    def wait_one_of_elements_text_loaded(self, locator, element_number=0):
        self.wait_full_upload()
        wait = WebDriverWait(self.driver, Configs.delay)
        one_of_elements = self.wait_one_of_elements_presence(locator, element_number)
        wait.until(lambda driver: one_of_elements.text.strip() != ''), \
        f"Text of one from elements with locator {locator}, not loaded"
        return one_of_elements

    def wait_all_elements_texts_loaded(self, locator):
        self.wait_full_upload()
        wait = WebDriverWait(self.driver, Configs.delay)
        elements = self.wait_all_elements_presence(locator)
        for element in elements:
            wait.until(lambda driver: element.text.strip() != ''),
            f"Text of one from elements with locator {locator}, not loaded"
        return elements

    # @allure.step("Wait load")
    def wait_full_upload(self):
        self.wait_for_page_load()
        # self.wait_for_ajax()
        # page_loader = self.xpath_locator(Locators.PAGE_LOADER)
        # preloader = self.xpath_locator(Locators.PRELOADER)
        # render_preloader = self.xpath_locator(Locators.RENDER_PRELOADER)
        # assert self.is_element_absent(page_loader), 'Page Loader still present'
        # assert self.is_element_absent(preloader), 'PreLoader still present'
        # assert self.is_element_absent(render_preloader), 'Render PreLoader still present'

    def wait_for_page_load(self):
        wait = WebDriverWait(self.driver, Configs.delay)
        wait.until(lambda driver: driver.execute_script('return document.readyState') == 'complete')

    def wait_for_ajax(self):
        wait = WebDriverWait(self.driver, Configs.delay)
        wait.until(lambda driver: driver.execute_script('return jQuery.active') == 0)

    @allure.step('Wait for Page load')
    def wait_for_page(self):
        locator = self.xpath_locator(Locators.PAGE_LOADER)
        try:
            self.wait_one_of_elements_presence(locator, short_delay=True)
            assert self.is_element_absent(locator)
        except WebDriverException:
            pass

    @allure.step('Wait for Preloader')
    def wait_for_preloader(self):
        locator = self.xpath_locator(Locators.PRELOADER)
        try:
            self.wait_one_of_elements_presence(locator, short_delay=True)
            assert self.is_element_absent(locator)
        except WebDriverException:
            pass

    @allure.step('Wait for Render')
    def wait_for_render(self):
        locator = self.xpath_locator(Locators.RENDER_PRELOADER)
        try:
            self.wait_one_of_elements_presence(locator, short_delay=True)
            assert self.is_element_absent(locator)
        except WebDriverException:
            pass

    def wait_for_frame_and_switch(self, locator):
        wait = WebDriverWait(self.driver, Configs.delay)
        return wait.until(ec.frame_to_be_available_and_switch_to_it(locator),
                          f"Can't switch to the frame {locator}")

    def wait_for_animation(self, locator):
        is_animation_in_progress = self.is_element_animated(locator)
        while is_animation_in_progress is True:
            time.sleep(0.5)
            is_animation_in_progress = self.is_element_animated(locator)

    ################################################# is_functions #####################################################

    def is_element_absent(self, locator, short_delay=False, long_delay=False):
        try:
            wait = self.delay(short_delay, long_delay)
            wait.until_not(ec.presence_of_element_located(locator),
                           f"Find element by locator {locator}")
            return True
        except TimeoutException:
            return False

    def is_element_invisible(self, locator, short_delay=False, long_delay=False):
        self.wait_full_upload()
        wait = self.delay(short_delay, long_delay)
        try:
            wait.until(ec.invisibility_of_element_located(locator), f"Can't find element by locator {locator}")
            return True
        except TimeoutException:
            return False

    def is_element_visible(self, locator, short_delay=False, long_delay=False):
        self.wait_full_upload()
        wait = self.delay(short_delay, long_delay)
        try:
            wait.until(ec.visibility_of_element_located(locator),
                       f"Can't find element by locator {locator}")
            return True
        except WebDriverException:
            return False

    def is_any_elements_visible(self, locator, short_delay=False, long_delay=False):
        self.wait_full_upload()
        wait = self.delay(short_delay, long_delay)
        try:
            wait.until(ec.visibility_of_any_elements_located(locator),
                       f"Can't find any elements by locator {locator}")
            return True
        except WebDriverException:
            return False

    def is_all_elements_visible(self, locator, short_delay=False, long_delay=False):
        self.wait_full_upload()
        wait = self.delay(short_delay, long_delay)
        try:
            wait.until(ec.visibility_of_all_elements_located(locator),
                       f"Can't find elements by locator {locator}")
            return True
        except WebDriverException:
            return False

    def is_alert_present_accept(self):
        wait = self.delay(short_delay=True)
        try:
            wait.until(ec.alert_is_present())
            alert = self.driver.switch_to.alert
            alert.accept()
            print("alert accepted")
        except WebDriverException:
            print("no alert")

    def is_toggle_switched(self, toggle_status_locator):
        toggle_status_elem = self.wait_one_of_elements_presence(toggle_status_locator)
        if toggle_status_elem.is_selected():
            return True
        else:
            return False

    def is_element_animated(self, locator):
        self.wait_full_upload()
        return self.driver.execute_script("return jQuery('" + locator + "').is(':animated');")

    @allure.step('File is created')
    def is_file_ready_for_download(self, ready_for_download_element_class):
        locator = self.xpath_locator(f"//*[contains(@class, '{ready_for_download_element_class}')]]")
        return self.is_element_visible(locator, long_delay=True)

    @allure.step('File is exist')
    def is_file_exist_in_download(self, file_name, delete=False):
        file_path = Configs.download_dir
        file = os.path.join(file_path, file_name)
        if delete and os.path.exists(file):
            os.remove(file)
            return True
        elif not delete and os.path.exists(file):
            return True
        else:
            return False

    ################################################## get_functions ###################################################

    def get_one_of_elements_attribute(self, locator, attribute='value', element_number=0):
        element = self.wait_one_of_elements_attribute_loaded(locator, attribute, element_number)
        attribute_value = element.get_attribute(attribute)
        return attribute_value

    def get_one_of_elements_text(self, locator, element_number=0):
        element = self.wait_one_of_elements_text_loaded(locator, element_number)
        attribute_text = element.text
        return attribute_text

    ################################################# check_functions ##################################################

    def check_one_of_elements_attribute(self, locator, expected, contains=False, attribute='value',
                                        element_number=0):
        elements = self.wait_all_elements_attributes_loaded(locator, attribute)
        element = elements[element_number]
        actual = element.get_attribute(attribute)
        if not contains:
            return True if expected == actual else False
        elif contains:
            return True if contains in actual else False

    def check_all_elements_attribute(self, locator, attribute, expected):
        elements = self.wait_all_elements_attributes_loaded(locator, attribute)
        elements_list = [elem.get_attribute(attribute) for elem in elements]
        for elem in elements_list:
            actual = elem
            if expected in actual:
                continue
            elif expected not in actual:
                return False
        return True

    def check_one_of_elements_text(self, locator, expected, element_number=0, split_option=False, split_position=0):
        actual_text_part = None
        elements = self.wait_all_elements_texts_loaded(locator)
        element = elements[element_number]
        actual_text = element.text
        if split_option:
            actual_text_part = actual_text.split(split_option)[split_position]
        return True if actual_text == expected or actual_text_part == expected else False

    def check_all_elements_text(self, locator, expected):
        elements = self.wait_all_elements_texts_loaded(locator)
        elements_texts_list = [elem.text for elem in elements]
        for elem_text in elements_texts_list:
            actual = elem_text
            if expected == actual:
                continue
            elif expected != actual:
                return False
        return True

    ################################################# fill_functions ###################################################

    def fill(self, locator, text, enter=False):
        element = self.wait_element_clickable(locator)
        element.click()
        element.clear()
        element.send_keys(text)
        if enter:
            element.click()
            element.send_keys(Keys.ENTER)

    def fill_one_search_dropdown(self, dropdown_locator, option, part_option=False, checkbox=False):
        option_to_select = option if not part_option else part_option
        option_locator = self.xpath_locator(f"//span[text()='{option_to_select}']")
        dropdown = self.wait_element_clickable(dropdown_locator)
        self.hover(dropdown_locator, click=True)
        dropdown.clear()
        dropdown.send_keys(option_to_select)
        assert self.is_element_visible(option_locator), f'Expected that {option} appear'
        self.hover(option_locator, click=True)
        self.wait_full_upload()
        if checkbox:
            self.hover(dropdown_locator, click=True)
            dropdown.send_keys(Keys.ESCAPE)
        assert self.check_one_of_elements_attribute(dropdown_locator, option), \
            f'Expected that dropdown has value - {option}'

    def fill_few_search_dropdown(self, dropdown_locator, options):
        dropdown = self.wait_element_clickable(dropdown_locator)
        joined_options = ", "
        for option in options:
            self.hover(dropdown_locator, click=True)
            dropdown.clear()
            dropdown.send_keys(option)
            option_locator = self.xpath_locator(f"//span[text()='{option}']")
            assert self.is_element_visible(option_locator), f'Expected that {option} appear'
            self.hover(option_locator, click=True)
            self.wait_full_upload()
            joined_options.join(option)
        self.hover(dropdown_locator, click=True)
        dropdown.send_keys(Keys.ESCAPE)
        assert self.check_one_of_elements_attribute(dropdown_locator, expected=joined_options), \
            f'Expected that dropdown has value - {joined_options}'

    def fill_set(self, locator, text):
        element = self.wait_element_clickable(locator)
        element.click()
        self.driver.execute_script("arguments[0].setAttribute('value',arguments[1])", element, text)

    def fill_drag_and_drop(self, locator_from, locator_to):
        element_from = self.wait_element_clickable(locator_from)
        locator_to = self.wait_element_clickable(locator_to)
        ActionChains(self.driver).drag_and_drop(element_from, locator_to).perform()

    def fill_all_elements(self, locator, text):
        elements = self.wait_all_elements_visibility(locator)
        for element in elements:
            element.click()
            element.clear()
            element.send_keys(text)

    ################################################# select_functions #################################################

    def select_action_from_dropdown(self, dropdown_locator, options_locator, option=False, custom_option=False):
        self.hover(dropdown_locator, click=True)
        self.wait_all_elements_visibility(options_locator)
        option_locator = self.xpath_locator(f"//p[text()='{option}']") if option else custom_option
        option = self.wait_element_clickable(option_locator)
        option.click()

    def select_one_from_dropdown(self, dropdown_locator, options_locator, option, custom_option=False, text=False):
        self.hover(dropdown_locator, click=True)
        self.wait_all_elements_visibility(options_locator)
        option_locator = self.xpath_locator(f"//span[text()='{option}']") if not custom_option else custom_option
        option_element = self.wait_element_clickable(option_locator)
        option_element.click()
        if text:
            assert self.check_one_of_elements_text(dropdown_locator, option), \
                f'Expected that dropdown {dropdown_locator} has a text {option}'
        else:
            assert self.check_one_of_elements_attribute(dropdown_locator, option), \
                f'Expected that dropdown {dropdown_locator} has a value {option}'

    def select_few_from_dropdown(self, dropdown_locator, options_locator, options, custom_options=False, text=False):
        self.hover(dropdown_locator, click=True)
        self.wait_all_elements_visibility(options_locator)
        for option in options:
            if not custom_options:
                option_locator = self.xpath_locator(f"//span[text()='{option}']")
            else:
                option_locator = self.xpath_locator(str(custom_options).format(option))
            option_element = self.wait_element_clickable(option_locator)
            option_element.click()
        self.hover(dropdown_locator, click=True)
        self.wait_full_upload()
        joined_options = ", ".join(options)
        if text:
            assert self.check_one_of_elements_text(dropdown_locator, joined_options), \
                f'Expected that dropdown {dropdown_locator} has a text {joined_options}'
        else:
            assert self.check_one_of_elements_attribute(dropdown_locator, expected=joined_options), \
                f'Expected that dropdown {dropdown_locator} has a value {joined_options}'

    def switch_focus(self, css_locator):
        self.driver.execute_script(f"document.querySelector('{css_locator}').focus()")

    def switch_toggle(self, toggle_status_locator, toggle_switch_locator):
        toggle_switch_elem = self.wait_element_clickable(toggle_switch_locator)
        if not self.is_toggle_switched(toggle_status_locator):
            toggle_switch_elem.click()
        else:
            print('Toggle is already enabled')

    ################################################# hover_scroll_functions ###########################################

    def hover(self, locator, click=False):
        element = self.wait_element_clickable(locator)
        action = ActionChains(self.driver).move_to_element(element)
        if click:
            action.click().perform()
        else:
            action.perform()

    def scroll_to_element(self, locator):
        element = self.wait_one_of_elements_visibility(locator)
        self.driver.execute_script("arguments[0].scrollIntoView();", element)

    ################################################# other_functions ##################################################

    @allure.step('Open page')
    def open_page(self, page):
        self.driver.get(Configs.url + f'/{page}')

    @allure.step('Open user page')
    def open_user_page(self, user_email, user_password, page):
        cookies, user_data = get_user_data_from_login(user_email, user_password)
        clean_cookies = []
        session = {'name': 'session', 'value': cookies['session']}
        clean_cookies.append(session)
        token = {'name': 'token', 'value': user_data['token']}
        clean_cookies.append(token)

        # Enables network tracking to use Network.setCookie method
        self.driver.execute_cdp_cmd('Network.enable', {})

        # Iterate through dict and add all the cookies
        for cookie in clean_cookies:
            if cookie['name'] == 'session':
                cookie.update({'domain': Configs.cookie_domain})
            self.driver.execute_cdp_cmd('Network.setCookie', cookie)

        # Disable network tracking
        self.driver.execute_cdp_cmd('Network.disable', {})

        # Open page
        self.open_page(page)

    @allure.step('Refresh page')
    def driver_refresh(self):
        self.driver.refresh()

    def add_screenshot(self, name):
        allure.attach(self.driver.get_screenshot_as_png(), name=name, attachment_type=allure.attachment_type.PNG)

    @allure.step('Clearing HTTP requests')
    def clean_requests(self):
        del self.driver.requests
        time.sleep(1)

    def xpath_locator(self, locator):
        return (By.XPATH, locator)

    def css_locator(self, locator):
        return (By.CSS_SELECTOR, locator)

    def delay(self, short_delay=False, long_delay=False):
        if short_delay:
            wait = WebDriverWait(self.driver, Configs.short_delay)
        elif long_delay:
            wait = WebDriverWait(self.driver, Configs.long_delay)
        else:
            wait = WebDriverWait(self.driver, Configs.delay)
        return wait

    def time_sleep(self, s):
        wait = WebDriverWait(self.driver, s + 1)
        deadline = time.monotonic() + s
        wait.until(WaitSleep(deadline), 'Sleep error')


class WaitSleep:

    def __init__(self, deadline):
        self._deadline = deadline

    def __call__(self, atr):
        if time.monotonic() >= self._deadline:
            return True
        elif time.monotonic() >= self._deadline:
            return False
