import time
import json
import allure
import imaplib
import requests
from src.global_configs import Configs


class EmailHelper:

    ############################################# GMail ################################################################

    @allure.step('Get last received Email from Gmail')
    def get_last_gmail(self, address, email_address, password, expected_subject):
        mail = imaplib.IMAP4_SSL(address)
        mail.login(email_address, password)

        email_bytes = False
        error = False

        for t in range(0, Configs.long_delay):
            mail.list()
            mail.select("INBOX")
            result, search_result = mail.search(None, f'(SUBJECT "{expected_subject}")')
            if len(search_result[0]) == 0:
                error = True
                time.sleep(1)
            elif len(search_result[0]) > 0:
                error = False
                email_bytes = search_result[0]
                break
        if error:
            raise Exception(f'Email {expected_subject} not received')

        result, email = mail.fetch(email_bytes, '(RFC822)')
        email_content = email[0][1]
        email_content_string = email_content.decode('utf-8')

        mail.close()
        mail.logout()
        return email_content_string

    @allure.step('Clean Gmail')
    def clean_gmail(self, address, email, password):
        mail = imaplib.IMAP4_SSL(address)
        mail.login(email, password)

        mail.list()
        mail.select("INBOX")
        result, data = mail.search(None, 'ALL')

        for mail_id in data[0].split():
            mail.store(mail_id, '+FLAGS', '\\Deleted')

        mail.expunge()

        mail.close()
        mail.logout()

    ########################################### Temp Email #############################################################

    @allure.step('Get last received Email from Temp Email')
    def get_temp_email(self, email_address, expected_subject):
        temp_email_api = Configs.temp_email_api
        path_get = f'{temp_email_api}?email={email_address}&format=json'
        print("API.get Temp email path " + path_get)
        for t in range(0, Configs.long_delay):
            print("API.get Temp email path " + path_get)
            response_get = requests.get(path_get)
            print("API. get Temp email status " + str(response_get))
            email_data = json.loads(response_get.text)
            emails_len = len(email_data)
            if emails_len > 0:
                for e in email_data:
                    subject = e['subject']
                    if subject == expected_subject:
                        return email_data[0]['html']
            elif emails_len == 0:
                time.sleep(1)
        raise Exception(f'Email {expected_subject} not received')

    @allure.step('Clean Temp Email')
    def clean_temp_email(self, email_address):
        temp_email_api = Configs.temp_email_api
        path_delete = f'{temp_email_api}?email={email_address}&action=delete'
        response_delete = requests.get(path_delete)
        print("API.Email delete status " + str(response_delete))

        # Check Delete
        path_get = f'{temp_email_api}?email={email_address}&format=json'
        response_get = requests.get(path_get)
        print("API.Email get status " + str(response_get))
        email_data = json.loads(response_get.text)
        assert len(email_data) == 0, 'Expected that Temp Email is clear'
