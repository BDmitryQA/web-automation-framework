import allure
from selenium.webdriver.common.by import By
from src.pages.page import BasePage


class Locators:
    NAME = (By.XPATH, "//*[@id='name']")
    WEBSITE_LINK = (By.XPATH, "//*[@id='website']/..//a")
    INCORRECT_LOCATOR = (By.XPATH, "//*[@id='user_menu']/*[@class='logo_in']")


class MainPageHelper(BasePage):

    @allure.step('Main page is opened')
    def is_opened(self):
        assert self.is_element_visible(Locators.NAME), 'Expected that Main page is opened'

    ######################################## general_functions #################################################

    @allure.step('Some other nested step')
    def some_other_step_nested(self):
        self.some_other_step()
        self.some_other_step()
        self.some_other_step()

    ###################################### functions_with_elements ###############################################

    @allure.step('Click on non existent button')
    def click_on_non_existent_button(self):
        button = self.wait_element_clickable(Locators.INCORRECT_LOCATOR)
        button.click()

    ########################################## other_functions ###################################################

    @allure.step('Some other step')
    def some_other_step(self):
        pass

    @allure.step('Some other step with parameter')
    def some_other_step_parameter(self, *parameters):
        for parameter in parameters:
            print(parameter)

    @allure.step('Some other step with parameters')
    def some_other_step_parameters(self, param, param1):
        print(param)
        print(param1)

    ######################################### verifications ######################################################

    @allure.step('Verify site link')
    def verify_website_link(self, link):
        assert self.check_one_of_elements_attribute(Locators.WEBSITE_LINK, link, attribute="href"), \
            f'Expected that link is {link}'

    @allure.step('Verification Passed')
    def verify_pass(self):
        assert True

    @allure.step('Verification Failed')
    def verify_failed(self):
        assert False, 'Some error happened'
