import allure
import time
from selenium.webdriver.common.by import By
from src.pages.page import BasePage


class Locators:
    VIDEO_MODAL_TITLE = (By.XPATH, "")
    YOUTUBE_FRAME = (By.TAG_NAME, 'iframe')
    YOUTUBE_PLAYER = (By.ID, 'movie_player')


class VideoHelper(BasePage):

    ########################################### page_functions #########################################################

    @allure.step('Verify that video modal is opened')
    def is_video_modal_opened(self):
        return self.is_element_visible(Locators.VIDEO_MODAL_TITLE)

    ######################################## other_functions ###########################################################

    @allure.step('Get and Analyze youtube state')
    def get_and_analyze_video_status(self):
        status = self.get_video_status()
        self.analyze_video_status(status)

    @allure.step('Get YouTube status')
    def get_video_status(self):
        with allure.step('Switch to YouTube frame'):
            self.wait_for_frame_and_switch(Locators.YOUTUBE_FRAME)
        with allure.step('Get Youtube status'):
            self.wait_one_of_elements_presence(Locators.YOUTUBE_PLAYER)
            status = self.driver.execute_script("return document.getElementById('movie_player').getPlayerState()")
        with allure.step('Back to main frame'):
            self.driver.switch_to.default_content()
        if status:
            return status
        elif not status:
            raise Exception('Video status is None')

    @allure.step('Analyze youtube status')
    def analyze_video_status(self, status):
        if status == -1:
            with allure.step('Video is not started'):
                pass
        elif status == 0:
            with allure.step('Video is ended'):
                pass
        elif status == 1:
            with allure.step('Video is playing'):
                pass
        elif status == 2:
            with allure.step('Video is paused'):
                pass
        elif status == 3:
            with allure.step('Video is buffering'):
                pass
        elif status == 5:
            with allure.step('Video is cued'):
                pass
        else:
            with allure.step('Unknown video status'):
                raise Exception('Unknown video status')

    ######################################### verifications ############################################################

    @allure.step('Verify that video is playing')
    def check_video(self, attempts):
        for t in range(0, attempts):
            self.get_and_analyze_video_status()
            time.sleep(5)

    @allure.step('Video modal status')
    def check_video_without_iframe(self, s):
        deadline = time.monotonic() + s
        while time.monotonic() <= deadline:
            try:
                assert self.is_video_modal_opened()
                time.sleep(5)
                with allure.step('Video modal still opened'):
                    pass
            except Exception as e:
                with allure.step('Video modal is unavailable'):
                    print(f'Video modal in unavailable - {e}')
                    break
