import requests
import json
from src.global_configs import Configs


def get_user_data_from_login(email, password):
    session, response = user_login(email, password)
    cookies = session.cookies.get_dict()
    user_data = json.loads(response.text)
    return cookies, user_data


def user_login(email, password):
    # Get Configs
    url = Configs.api
    api_ver = Configs.api_ver

    # Start session
    session = requests.Session()

    # Provider login
    path = url + api_ver + '/login'
    print("API. Login url " + str(path))
    credential = {'email': email, 'password': password}
    print("API. User credentials " + str(credential))
    response = session.post(path, json=credential)
    print("API.User login response " + str(response))
    return session, response




