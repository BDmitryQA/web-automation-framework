import os
import shutil

# Read more - https://docs.pytest.org/en/stable/usage.html


# ONLY FOR LINUX LOCAL TESTING
def test_stability():
    root_dir = (os.path.dirname(os.path.dirname(__file__)))
    src_dir = os.path.join(root_dir, 'src')

    reports_dir = os.path.join(src_dir, 'reports')
    allure_dir = os.path.join(reports_dir, 'allure-results')

    test_dir = os.path.join(src_dir, 'tests')
    test_suite_dir = os.path.join(test_dir, 'test_suite')
    test_suite_file = os.path.join(test_dir, 'test_ui.py')
    test_name = 'test_website_link'

    treads = 2

    try:
        shutil.rmtree(allure_dir)
    except OSError as e:
        print("Error: %s : %s" % (allure_dir, e.strerror))

    for t in range(0, 10):
        # Run all tests
        # status = os.system(f'pytest -n{treads} --alluredir={allure_dir} {test_dir}')

        # Run test with mark
        # status = os.system('pytest -n{treads} -m ui --alluredir={allure_dir} {test_dir}')

        # Select Test Suite folder to run
        # status = os.system(f'pytest -n{treads} --alluredir={allure_dir} {test_suite_dir}::"TestSuite"')

        # Select Test Suite file to run
        # status = os.system(f'pytest -n{treads} --alluredir={allure_dir} {test_suite_file}::"TestSuite"')

        # Select Test to run
        status = os.system(f'pytest -n{treads} --alluredir={allure_dir} {test_suite_file}::"TestSuite"::{test_name}')

        # End of Tests
        if status != 0:
            print('Error')
            assert False, f'Error during {t} run'
