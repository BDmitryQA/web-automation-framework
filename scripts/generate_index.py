import os
import json
import sys

INDEX_TEXT_START = """<!DOCTYPE html>
<html>
<head><title>Index of {folderPath}</title></head>
<body>
    <h2>Index of {folderPath}</h2>
    <hr>
    <ul>
        <li>
            <a href='../'>../</a>
        </li>
"""
INDEX_TEXT_END = """
    </ul>
</body>
</html>
"""


def folders_analysis(branch_name):
    branches_list = []
    jobs_list = []

    # Get list of flies from file
    folder = os.path.dirname(__file__)
    file = os.path.join(folder, 'file_list.txt')
    with open(file, "r") as f:
        data = f.read()
        file_json = json.loads(data)
        list_json = file_json

    # Search folders
    for value in list_json:
        split_path = value['Key'].split('/')
        if len(split_path) >= 3 and split_path[0] != '':
            if 'job_' in split_path[1]:
                branch = split_path[0]
                if branch not in branches_list:
                    branches_list.append(branch)
            if 'job_' in split_path[1] and branch_name in split_path[0]:
                job = split_path[1]
                if job not in jobs_list:
                    jobs_list.append(job)
    return branches_list, jobs_list


def generate_index(list_of_folders, branch=None):
    index_text = None
    type_dir = None

    # Set tab title and page title
    if branch is None:
        print("Indexing Branches")
        index_text = INDEX_TEXT_START.format(folderPath='Branches')
    elif branch is not None:
        print(f"Indexing {branch} Branch ")
        index_text = INDEX_TEXT_START.format(folderPath=f'{branch} Branch')

    # Add links to the list
    for folder in sorted(list_of_folders, reverse=True):
        index_text += "\t\t<li>\n\t\t\t<a href='" + folder + "'>" \
            + folder \
            + "</a>\n\t\t</li>\n"
    index_text += INDEX_TEXT_END
    src_dir = os.path.dirname(__file__)

    # Separate the different index files
    if branch is None:
        type_dir = os.path.join(src_dir, f'branches_index')
        print(f"Dir = {type_dir}")
    elif branch is not None:
        type_dir = os.path.join(src_dir, f'jobs_index')
        print(f"Dir = {type_dir}")

    # Create index folder and file
    if not os.path.isdir(type_dir):
        os.mkdir(type_dir)
    with open(f'{type_dir}/index.html', "w+") as index:
        index.write(index_text)
    print("Done")


current_branch = sys.argv[1]

if __name__ == '__main__':
    branches, jobs = folders_analysis(current_branch)
    generate_index(branches)
    generate_index(jobs, current_branch)
