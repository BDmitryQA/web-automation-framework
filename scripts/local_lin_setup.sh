# Requirements:
# from local_{OS}_setup_dependencies.sh

#- update packages
sudo apt-get -y update

#- Java
sudo apt -y install default-jdk

#- Google Chrome
wget "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
sudo dpkg -i ./google-chrome-stable_current_amd64.deb
rm ./google-chrome-stable_current_amd64.deb

#- Allure command line
wget "https://github.com/allure-framework/allure2/releases/download/2.17.2/allure_2.17.2-1_all.deb"
sudo dpkg -i ./allure_2.17.2-1_all.deb
rm ./allure_2.17.2-1_all.deb

#- Web driver
python3 ./web-automation-framework/scripts/install_driver.py
#- if you will see the error make sure that you inside virtualenv and run this command again
