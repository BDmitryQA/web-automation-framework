aws configure set aws_access_key_id $AWS_API_KEY
aws configure set aws_secret_access_key $AWS_API_SECRET
aws configure set region eu-west-3
aws configure set output json