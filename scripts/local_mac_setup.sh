# Requirements:
# from local_{OS}_setup_dependencies.sh

brew update

#- Java
brew install openjdk@8
sudo ln -sfn /usr/local/opt/openjdk@8/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk-8.jdk

#- Google Chrome
brew install --cask google-chrome

#- Allure command line
brew install allure

#- Web driver
python3 ./scripts/install_driver.py
#- if you will see the error make sure that you inside virtualenv and run this command again
