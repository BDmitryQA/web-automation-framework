# Requirements:
#- brew - https://brew.sh
#- git - https://git-scm.com/downloads
#- PyCharm - https://www.jetbrains.com/pycharm

#- update packages
brew update

#- Python 3.8
brew install python@3.8

#- pip
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 get-pip.py

#- venv
pip3 install virtualenv

#- Docker and Docker-compose
brew install --cask docker
