import os
from datetime import datetime


# FOR CI RUN OR LOCAL EXECUTIONS FROM TERMINAL\IDE
def add_to_logger(driver, action, start=False):
    root_dir = (os.path.dirname(os.path.dirname(__file__)))
    src_dir = os.path.join(root_dir, 'src')
    logs_dir = os.path.join(src_dir, 'logs')
    crash_screenshots_dir = os.path.join(logs_dir, 'crash_screenshots')
    log_name = 'crash_log.txt'
    log_top_name = 'top_log.txt'
    log_ctop_name = 'continues_top_log.txt'
    log_file_path = os.path.join(logs_dir, f'{log_name}')
    log_top_path = os.path.join(logs_dir, f'{log_top_name}')
    log_ctop_path = os.path.join(logs_dir, f'{log_ctop_name}')

    if not os.path.isdir(logs_dir):
        os.mkdir(logs_dir)
    if not os.path.isdir(crash_screenshots_dir):
        os.mkdir(crash_screenshots_dir)

    current_time = datetime.now()
    time = current_time.strftime('%Y-%m-%d_%H:%M:%S')

    if not os.path.isfile(log_file_path):
        with open(log_file_path, 'w') as f:
            f.write(f"{time} - {action}\n")
    elif os.path.isfile(log_file_path):
        with open(log_file_path, 'a') as f:
            f.write(f"{time} - {action}\n")

    driver.save_screenshot(os.path.join(crash_screenshots_dir, f'{time}_{action}.png'))
    os.system(f'top -n 1 -b >> {log_top_path}')

    if start is not False:
        os.system(f'while true; do top -n 1 -b >> {log_ctop_path}; sleep 5; done &')
