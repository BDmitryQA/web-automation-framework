import os
import sys
# import gitlab
from slack_sdk.webhook import WebhookClient

# Read More about GitLab Slack integration - https://docs.gitlab.com/ee/user/project/integrations/slack.html
# Read More about Python-GitLab - https://python-gitlab.readthedocs.io/en/stable/index.html
# Read More about Slack-SDK- https://slack.dev/python-slack-sdk/index.html


def send_to_slack_reports(job_id):
    # Get status from gitlab
    # Login to GitLab
    # gl = gitlab.Gitlab(private_token=os.environ['SLACK_TOKEN'])

    # Get Project
    # project = gl.projects.get(os.environ['CI_PROJECT_ID'])

    # Get status from Job and send notification
    # clean_job_id = job_id.split('_')[1]
    # job = project.jobs.get(clean_job_id)
    # if job.status == 'failed':
    #     # Send to Slack
    #     hook_url = os.environ.get('SLACK_HOOK')
    #     webhook = WebhookClient(hook_url)
    #
    #     response = webhook.send(text=f"Some of UI Auto Tests are Failed. Please open the report - "
    #                                  f"{os.environ['REPORT_FULL_LINK']}")
    #     assert response.status_code == 200
    #     assert response.body == "ok"
    #     raise Exception('Some of the Auto Tests are Failed')

    # Get status from file
    root_dir = (os.path.dirname(os.path.dirname(__file__)))
    src_dir = os.path.join(root_dir, 'src')
    logs_dir = os.path.join(src_dir, 'logs')
    log_status_path = os.path.join(logs_dir, 'status')
    try:
        with open(log_status_path, 'r') as f:
            status = f.read()
        if 'failed' in status:
            # Send to Slack
            hook_url = os.environ.get('SLACK_HOOK')
            webhook = WebhookClient(hook_url)

            response = webhook.send(text=f"Some of UI Auto Tests are Failed. Please open the report - "
                                         f"{os.environ['REPORT_FULL_LINK']}")
            assert response.status_code == 200
            assert response.body == "ok"
            raise Exception('Some of the Auto Tests are Failed')
        else:
            print(f'Unexpected status {status}')
    except FileNotFoundError:
        print('All Auto Tests passed. Notification not needed')


gitlab_job_id = sys.argv[1]

if __name__ == '__main__':
    send_to_slack_reports(gitlab_job_id)
