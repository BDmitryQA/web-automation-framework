import os
from webdriver_manager.chrome import ChromeDriverManager


def driver_install():
    executable_path = ChromeDriverManager().install()
    driver_path = os.path.join(os.path.dirname(__file__), '../src/driver_path.txt')
    with open(driver_path, 'w') as f:
        f.write(executable_path)


if __name__ == '__main__':
    driver_install()
