import os
import json
import datetime
from multiprocessing.pool import ThreadPool as Pool


def perform_folder_list_upload():
    pool_size = 5
    pool = Pool(pool_size)

    get_list_of_latest_files()
    list_json = open_list_of_latest_files()
    last_jobs_list = get_last_jobs_from_json(list_json, 5)
    folders_for_download = create_list_for_download_and_index(list_json, last_jobs_list)
    for folder in folders_for_download:
        pool.apply_async(download_folder, (folder,))
    pool.close()
    pool.join()


def open_list_of_latest_files():
    with open('json_list.txt', 'r') as f:
        json_list = f.read()
    list_json = json.loads(json_list)
    return list_json


def get_last_jobs_from_json(list_json, jobs_amount):
    jobs_list = []

    for value in list_json:
        split_path = value['Key'].split('/')
        if len(split_path) >= 3 and split_path[0] != '':
            if 'pipeline_' in split_path[1]:
                job = split_path[1]
                if job not in jobs_list:
                    jobs_list.append(job)
    jobs_list.sort(reverse=True)
    last_jobs_list = jobs_list[:jobs_amount]
    return last_jobs_list


def create_list_for_download_and_index(list_json, last_jobs_list):
    files_list = []
    folders_for_download = []

    for job in last_jobs_list:
        for value in list_json:
            if job in value['Key']:
                if value['Key'] not in files_list:
                    files_list.append(value['Key'])
                path_elements = value['Key'].split('/')[:2]
                folder = path_elements[0]+'/'+path_elements[1]
                if folder not in folders_for_download:
                    folders_for_download.append(folder)
    with open('file_list.txt', 'a') as f:
        for item in files_list:
            f.write('%s\n' % item)
    return folders_for_download


def get_last30days():
    last30days = datetime.date.today()
    last30days += datetime.timedelta(days=-30)
    return str(f'"{last30days}"')


def get_list_of_latest_files():
    last_30_days = get_last30days()
    os.system(f"aws s3api list-objects-v2 --bucket $S3_RESULTS_BUCKET_NAME --query 'Contents[?LastModified>=`'{last_30_days}'`]' > json_list.txt")


def download_folder(folder):
    os.system(f"aws s3 cp s3://$S3_RESULTS_BUCKET_NAME/{folder}/ ./downloaded_history/{folder}/ --recursive")


if __name__ == '__main__':
    perform_folder_list_upload()
