# Docker Container for Auto Tests

FROM python:3.8
ENTRYPOINT ["tail", "-f", "/dev/null"]

WORKDIR /web-automation-framework
COPY ./ /web-automation-framework/

# Set URL
ENV WEBAPP_URL="http://api:3000/app"

# Upgrade pip
RUN pip install --upgrade pip

# We need wget to set up the PPA and xvfb to have a virtual screen and unzip to install the Chromedriver
RUN apt-get -y update
RUN apt-get install -y wget xvfb unzip

# Set up the Chrome PPA
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list

# Update the package list and install chrome
RUN apt-get update -y
RUN apt-get install -y google-chrome-stable
RUN google-chrome --version

# Install requirements and get driver
RUN pip install -r requirements.txt
RUN python /web-automation-framework/scripts/install_driver.py