Web Auto Test Framework with stack Python+PyTest+Selenium+Allure <br/> 
Execution GitLabCi+Docker+AWS S3 <br/> 

<br/>
Supported OS: <br/>
- Ubuntu 18.04 and newer <br/>
- Windows 10 and newer** <br/>
- MacOS 11 and newer***  <br/>
** commands from .sh scripts and Makefile must be run manually on Windows <br/>
*** only Mac with Intel chipset <br/>

OVERVIEW
=======================================
There are three ways to run Auto Tests. <br/>
1. Run in GitLab Ci, to verify that everything is okay with all last changes and to save results with history on AWS S3. <br/>
2. Local Run in Docker container, to verify that everything is okay with your cages before push. <br/>
3. Local Run from terminal or IDE, to debug Failed AutoTests and to investigate Test flow. <br/>

LOCAL SETUP
=======================================
SETUP TO CLEAN PC or LAPTOP
-------------------------
0. Only for Mac. Install brew - https://brew.sh <br/>
1. Install git - https://git-scm.com/downloads <br/>
   F.e for the Ubuntu just run in the terminal: sudo apt -y install git-all <br/>
2. Install IDE <br/>
   F.e PyCharm - https://www.jetbrains.com/pycharm <br/>
3. Open IDE and get project from VCS <br/>
5. After project will be downloaded, please do not create virtual env. Open terminal and run next command: <br/>
   for Linux: bash ./web-automation-framework/scripts/local_lin_setup_dependencies.sh <br/>
   for Mac: bash ./web-automation-framework/scripts/local_mac_setup_dependencies.sh <br/>
   Note: for the Mac, brew is required <br/>
   Note: for the Windows, you need to install manually - Python 3.8, pip, venv <br/>

SETUP FOR LOCAL RUN IN DOCKER
-------------------------
You need only project and Docker with Docker-Compose <br/>
1. Rise web app and Auto Tests in the containers: <br/>
   sudo make create_containers <br/>
   Note: for the Mac, you need to open Docker manually before this command execution <br/>
   Note: for the Windows, you need to run commands from makefile manually and open Docker manually before commands execution<br/>
   Note: if you change something in the web app you need to run sudo make create_api_container <br/>
   Note: if you change something in the auto tests you need to run sudo make create_at_container <br/>

SETUP FOR LOCAL RUN FROM TERMINAL OR IDE
-------------------------
If you Already have brew (Mac), git, IDE, Python 3.8, pip, venv, webapp project locally, Docker and Docker-compose <br/>
<br/>
1. Open project in IDE <br/>
2. Create virtual env and set up all requirements through the IDE <br/>
   Additional information for the PyCharm - https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html <br/>
3. After requirements will be installed open terminal and make sure that you inside virtualenv and in the root folder <br/>
4. Run next command: <br/>
   for Linux: bash ./web-automation-framework/scripts/local_lin_setup.sh <br/>
   for Mac: bash ./web-automation-framework/scripts/local_mac_setup.sh <br/>
   Note: for the Mac, brew is required <br/>
   Note: for the Windows, you need to install manually - Java, Docker, Docker compose, Google Chrome, Allure command line, Web driver(via script) <br/>

LOCAL RUN
=======================================
LOCAL RUN IN A DOCKER
-------------------------
1. Make sure that web app and auto tests containers is started <br/>
2. Run next command to run AutoTests: <br/>
   sudo make local_auto_tests_run <br/>
   Note: for the Windows, you need to run Docker commands from the Make file manually <br/>

VIEW RESULTS
-------------------------
1. Open link http://localhost:8081/

RUN FROM TERMINAL OR IDE
-------------------------
Change mode to "normal" if you need to see all test flow in Config file. Not available in the containers or Ci. <br/>
Make sure that web app is started <br/>

To execute from terminal: <br/>
1. Make sure that web app container is started
2. Open terminal. Make sure that you inside virtualenv and in the root folder. Run next command: <br/>
   bash ./scripts/local_run.sh <br/>
   Note: for the Windows you need to run commands from the file manually <br/>
   Additional information about pytest syntax: <br/>
   pytest 'test_file'::'test_class'::'test' <br/>
   More: https://docs.pytest.org/en/stable/usage.html <br/>

To execute from IDE <br/>
1. Set up pytest as default test runner <br/>
2. Make sure that you are inside virtualenv <br/>
3. To execute tests from ./src/tests via IDE use configuration: <br/>
   <br/>
   Target Module Name: src.tests <br/>
   Additional arguments: -n{$TREADS_COUNT} --dist loadscope --alluredir=.\src\reports\allure-results <br/>
   <br/>
   where: <br/>
   -n{$TREADS_COUNT} .Xdist's argument for multiprocessing. Set Process count instead {$TREADS_COUNT} <br/>
   --dist loadscope  .Scope for each runner. One runner per each test class <br/>
   Additional information - https://pypi.org/project/pytest-xdist/#running-tests-across-multiple-cpus <br/>
   --alluredir=.\src\reports\allure-results .Folder where allure will save test results <br/>
   Additional information - https://docs.qameta.io/allure/#_usage <br/>

VIEW RESULTS
-------------------------
1. Open terminal. Make sure that you inside virtualenv.
2. To view the Report locally - run next command : <br/>
   bash ./scripts/local_report_generation.sh
   Note: for the Windows you need to run commands from the file manually <br/>
   Additional information - https://docs.qameta.io/allure/#_report_generation <br/>

CI RUN
========================
START TEST
-------------------------
1. Open GitLab web-automation-framework repository
2. Open GitLab CI\CD - Pipelines
3. Click "Run pipeline"
4. Select web-automation-framework branch
5. Click "Run pipeline"
<br/>
Important! Users must run only 1 pipeline per 1 branch in one time to avoid report history issues <br/>
Note: CI\CD pipeline also will be started after each push or merge command (of any branch) of web-automation-framework <br/>

VIEW RESULTS
-------------------------
1. Status of the CI\CD Pipeline is available on appropriate GitaLab page
2. Terminal report will be available in Pipeline Job, after pipeline is done
3. Detailed report will be available by the link, after pipeline is done - http://web-automation-framework-allure-reports.s3-website.eu-west-3.amazonaws.com/

MORE INFO:
========================
1. Python - https://www.python.org/about/
2. PyTest - https://docs.pytest.org/en/stable/contents.html
3. Selenium - https://selenium-python.readthedocs.io/index.html
4. SeleniumDev - https://www.selenium.dev/selenium/docs/api/java/overview-summary.html
5. selenium-wire - https://pypi.org/project/selenium-wire/  
6. Chrome driver - https://chromedriver.chromium.org/home
7. Chrome driver devtools - https://chromedevtools.github.io/devtools-protocol
8. Allure - https://docs.qameta.io/allure/#_about
9. GitLabC Ci - https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html
10. Docker  - https://docs.docker.com/engine/reference/run/
11. Docker-Compose - https://docs.docker.com/compose/reference/