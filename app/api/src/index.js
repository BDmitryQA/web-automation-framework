const express = require("express");
const app = express();

app.get("/app", (req, res) => {
	res.send(`<!DOCTYPE html>
<html>
   <head>
      <meta content="text/html; charset=UTF-8" http-equiv="content-type">
 <style type="text/css">@import url("https://themes.googleusercontent.com/fonts/css?kit=fpjTOVmNbO4Lz34iLyptLUXza5VhXqVC6o75Eld_V98");
    .lst-kix_rdf4hawgu4e9-8>li:before{
        content:"/0025a0 "
    }
    .lst-kix_rdf4hawgu4e9-3>li:before{
        content:"/0025cf "
    }
    .lst-kix_rdf4hawgu4e9-2>li:before{
        content:"/0025a0 "
    }
    .lst-kix_wypyv83gg393-8>li:before{
        content:"/0025a0 "
    }
    .lst-kix_wypyv83gg393-6>li:before{
        content:"/0025cf "
    }
    .lst-kix_wypyv83gg393-7>li:before{
        content:"/0025cb "
    }
    .lst-kix_rdf4hawgu4e9-4>li:before{
        content:"/0025cb "
    }
    .lst-kix_rdf4hawgu4e9-5>li:before{
        content:"/0025a0 "
    }
    .lst-kix_wypyv83gg393-4>li:before{
        content:"/0025cb "
    }
    .lst-kix_wypyv83gg393-5>li:before{
        content:"/0025a0 "
    }
    .lst-kix_rdf4hawgu4e9-7>li:before{
        content:"/0025cb "
    }
    .lst-kix_rdf4hawgu4e9-6>li:before{
        content:"/0025cf "
    }
    .lst-kix_wypyv83gg393-0>li:before{
        content:"/0025cf "
    }
    .lst-kix_wypyv83gg393-1>li:before{
        content:"/0025cb "
    }
    .lst-kix_wypyv83gg393-2>li:before{
        content:"/0025a0 "
    }
    .lst-kix_wypyv83gg393-3>li:before{
        content:"/0025cf "
    }
    ul.lst-kix_rdf4hawgu4e9-8{
        list-style-type:none
    }
    ul.lst-kix_rdf4hawgu4e9-7{
        list-style-type:none
    }
    ul.lst-kix_wypyv83gg393-0{
        list-style-type:none
    }
    ul.lst-kix_rdf4hawgu4e9-6{
        list-style-type:none
    }
    ul.lst-kix_wypyv83gg393-1{
        list-style-type:none
    }
    ul.lst-kix_rdf4hawgu4e9-5{
        list-style-type:none
    }
    ul.lst-kix_wypyv83gg393-6{
        list-style-type:none
    }
    ul.lst-kix_wypyv83gg393-7{
        list-style-type:none
    }
    ul.lst-kix_wypyv83gg393-8{
        list-style-type:none
    }
    ul.lst-kix_wypyv83gg393-2{
        list-style-type:none
    }
    ul.lst-kix_wypyv83gg393-3{
        list-style-type:none
    }
    ul.lst-kix_wypyv83gg393-4{
        list-style-type:none
    }
    ul.lst-kix_wypyv83gg393-5{
        list-style-type:none
    }
    ul.lst-kix_rdf4hawgu4e9-0{
        list-style-type:none
    }
    ul.lst-kix_rdf4hawgu4e9-4{
        list-style-type:none
    }
    ul.lst-kix_rdf4hawgu4e9-3{
        list-style-type:none
    }
    ul.lst-kix_rdf4hawgu4e9-2{
        list-style-type:none
    }
    ul.lst-kix_rdf4hawgu4e9-1{
        list-style-type:none
    }
    ul.lst-kix_pd8vdv4ssgs4-0{
        list-style-type:none
    }
    ul.lst-kix_pd8vdv4ssgs4-1{
        list-style-type:none
    }
    ul.lst-kix_pd8vdv4ssgs4-2{
        list-style-type:none
    }
    ul.lst-kix_pd8vdv4ssgs4-3{
        list-style-type:none
    }
    ul.lst-kix_pd8vdv4ssgs4-4{
        list-style-type:none
    }
    ul.lst-kix_pd8vdv4ssgs4-5{
        list-style-type:none
    }
    ul.lst-kix_pd8vdv4ssgs4-6{
        list-style-type:none
    }
    ul.lst-kix_pd8vdv4ssgs4-7{
        list-style-type:none
    }
    ul.lst-kix_pd8vdv4ssgs4-8{
        list-style-type:none
    }
    .lst-kix_pd8vdv4ssgs4-2>li:before{
        content:"- "
    }
    .lst-kix_pd8vdv4ssgs4-3>li:before{
        content:"- "
    }
    .lst-kix_pd8vdv4ssgs4-0>li:before{
        content:"- "
    }
    .lst-kix_pd8vdv4ssgs4-8>li:before{
        content:"- "
    }
    .lst-kix_pd8vdv4ssgs4-1>li:before{
        content:"- "
    }
    .lst-kix_rdf4hawgu4e9-1>li:before{
        content:"/0025cb "
    }
    .lst-kix_pd8vdv4ssgs4-7>li:before{
        content:"- "
    }
    .lst-kix_rdf4hawgu4e9-0>li:before{
        content:"/0025cf "
    }
    .lst-kix_pd8vdv4ssgs4-6>li:before{
        content:"- "
    }
    .lst-kix_pd8vdv4ssgs4-4>li:before{
        content:"- "
    }
    .lst-kix_pd8vdv4ssgs4-5>li:before{
        content:"- "
    }
    ol{
        margin:0;
        padding:0
    }
    table td,table th{
        padding:0
    }
    .c2{
        padding-top:0pt;
        padding-bottom:0pt;
        line-height:1.1500000000000001;
        orphans:2;
        widows:2;
        text-align:justify;
        margin-right:134.4pt
    }
    .c4{
        padding-top:12pt;
        padding-bottom:0pt;
        line-height:1.1500000000000001;
        page-break-after:avoid;
        orphans:2;
        widows:2;
        text-align:justify
    }
    .c22{
        padding-top:3pt;
        padding-bottom:11pt;
        line-height:1.1500000000000001;
        orphans:2;
        widows:2;
        text-align:left
    }
    .c7{
        padding-top:0pt;
        padding-bottom:0pt;
        line-height:1.1500000000000001;
        orphans:2;
        widows:2;
        text-align:justify
    }
    .c20{
        padding-top:14pt;
        padding-bottom:0pt;
        line-height:1.1500000000000001;
        orphans:2;
        widows:2;
        text-align:center
    }
    .c15{
        padding-top:0pt;
        padding-bottom:10pt;
        line-height:1.1500000000000001;
        orphans:2;
        widows:2;
        text-align:left
    }
    .c25{
        padding-top:14pt;
        padding-bottom:4pt;
        line-height:1.1500000000000001;
        orphans:2;
        widows:2;
        text-align:center
    }
    .c1{
        color:#000000;
        font-weight:400;
        text-decoration:none;
        vertical-align:baseline;
        font-family:"Calibri";
        font-style:normal
    }
    .c9{
        padding-top:0pt;
        padding-bottom:0pt;
        line-height:1.1500000000000001;
        orphans:2;
        widows:2;
        text-align:left
    }
    .c5{
        text-decoration-skip-ink:none;
        -webkit-text-decoration-skip:none;
        color:#1155cc;
        text-decoration:underline
    }
    .c6{
        vertical-align:baseline;
        font-style:normal;
        color:#000000;
        text-decoration:none
    }
    .c8{
        font-weight:700;
        font-size:14pt;
        font-family:"Calibri"
    }
    .c21{
        color:#212121;
        text-decoration:none;
        font-style:normal
    }
    .c28{
        max-width:510.2pt;
        padding:56.7pt 28.4pt 35.5pt 56.7pt
    }
    .c13{
        font-weight:700;
        font-family:"Calibri"
    }
    .c26{
        margin-left:173.1pt;
        height:11pt
    }
    .c3{
        color:inherit;
        text-decoration:inherit
    }
    .c12{
        font-size:11pt
    }
    .c18{
        font-weight:700
    }
    .c10{
        vertical-align:baseline
    }
    .c27{
        color:#0d0d0d
    }
    .c16{
        page-break-after:avoid
    }
    .c29{
        color:#212121
    }
    .c14{
        text-indent:36pt
    }
    .c0{
        font-size:12pt
    }
    .c11{
        font-weight:400
    }
    .c17{
        font-family:"Calibri"
    }
    .c23{
        font-size:13pt
    }
    .c24{
        font-family:"Liberation Sans"
    }
    .c19{
        background-color:#ffffff
    }
    .title{
        padding-top:24pt;
        color:#000000;
        font-weight:700;
        font-size:36pt;
        padding-bottom:6pt;
        font-family:"Calibri";
        line-height:1.1500000000000001;
        page-break-after:avoid;
        orphans:2;
        widows:2;
        text-align:left
    }
    .subtitle{
        padding-top:18pt;
        color:#666666;
        font-size:24pt;
        padding-bottom:4pt;
        font-family:"Georgia";
        line-height:1.1500000000000001;
        page-break-after:avoid;
        font-style:italic;
        orphans:2;
        widows:2;
        text-align:left
    }
    li{
        color:#000000;
        font-size:11pt;
        font-family:"Calibri"
    }
    p{
        margin:0;
        color:#000000;
        font-size:11pt;
        font-family:"Calibri"
    }
    h1{
        padding-top:24pt;
        color:#000000;
        font-weight:700;
        font-size:24pt;
        padding-bottom:6pt;
        font-family:"Calibri";
        line-height:1.1500000000000001;
        page-break-after:avoid;
        orphans:2;
        widows:2;
        text-align:left
    }
    h2{
        padding-top:12pt;
        color:#000000;
        font-size:14pt;
        padding-bottom:6pt;
        font-family:"Liberation Sans";
        line-height:1.1500000000000001;
        page-break-after:avoid;
        orphans:2;
        widows:2;
        text-align:left
    }
    h3{
        padding-top:14pt;
        color:#000000;
        font-weight:700;
        font-size:14pt;
        padding-bottom:4pt;
        font-family:"Calibri";
        line-height:1.1500000000000001;
        page-break-after:avoid;
        orphans:2;
        widows:2;
        text-align:left
    }
    h4{
        padding-top:12pt;
        color:#000000;
        font-weight:700;
        font-size:12pt;
        padding-bottom:2pt;
        font-family:"Calibri";
        line-height:1.1500000000000001;
        page-break-after:avoid;
        orphans:2;
        widows:2;
        text-align:left
    }
    h5{
        padding-top:11pt;
        color:#000000;
        font-weight:700;
        font-size:11pt;
        padding-bottom:2pt;
        font-family:"Calibri";
        line-height:1.1500000000000001;
        page-break-after:avoid;
        orphans:2;
        widows:2;
        text-align:left
    }
    h6{
        padding-top:10pt;
        color:#000000;
        font-weight:700;
        font-size:10pt;
        padding-bottom:2pt;
        font-family:"Calibri";
        line-height:1.1500000000000001;
        page-break-after:avoid;
        orphans:2;
        widows:2;
        text-align:left
    }
    </style>
      <title>Summary</title>
   </head>
   <body class="c19 c28">
      <h2 class="c16 c22">
         <span class="c13" id="name">DMYTRO BOHOLIUBOV </span>
      </h2>
      <img src="https://web-automation-framework-allure-reports.s3.eu-west-3.amazonaws.com/Avatar_Boholiubov.jpg" align="right" height="350px" width="397px" border="10px" alt="Loading" id="Avatar"/>
      <br>
      <span class="c0 c11 c17">QA since 09.01.2017</span>
      <br>
      <span class="c0 c18" id="location">Poland, Wroclaw.</span>
      <p class="c9">
         <span class="c18 c0">Email</span>
         <span class="c1 c0" id="email">: BDmitry1990@gmail.com</span>
      </p>
      <p class="c9">
         <span class="c18 c0">Phone number</span>
         <span class="c1 c0" id="phone">: on request</span>
      </p>
      <p class="c9">
         <span class="c6 c18 c0" id="website">Website: </span>
         <span class="c5 c0">
		 <a class="c3" href="http://qa-guide.net">qa-guide.net</a>
         </span>
         <br>
      </p>
      <p class="c9">
         <span class="c18 c0">Social Media</span>
         <span class="c0">:
         <br>
         </span>
         <span class="c5 c0">
         <a class="c3" href="https://www.linkedin.com/in/bdmitry1990/">Linkedin</a>
         </span>
         <span class="c0">
         <br>
         </span>
         <span class="c5 c0">
         <a class="c3" href="https://www.facebook.com/BDmitry1990/">Facebook</a>
         </span>
      </p>
      <h3 class="c25 c16">
         <span class="c6 c8">Objective</span>
      </h3>
      <p class="c15">
         <span class="c0 c19 c29">An experienced QA specialist (manual with more than 3 years experience/Automation (Python) with more than 2 years experience) is looking for an interesting, difficult, and useful project in an IT company for the full-time, full remote. </span>
      </p>
      <h3 class="c16 c25" id="skills">
         <span class="c6 c8">Qualifications (Skills)</span>
      </h3>
      <p class="c2">
         <span class="c1 c0">- knowledge of applications creating process (SDLC);</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- knowledge of development methodologies;</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- work in the Scrum team (Agile);</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- knowledge of testing theory;</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- experience with development of Test documentation (Test Plans, Checklists, Test Cases, different reports);</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- experience with performing functional testing, non-functional testing, change related testing;</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- experience with Web, Mobile and Desktop application testing as Manual specialist;</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- knowledge of SQL basic syntax (MySQL);</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- understanding of how web applications work (Client-server architecture, main protocols, etc.);</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- understanding of how front-end work (HTML, CSS, JS basic knowledge);</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- experience with using the Developer Console in web browsers;</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- experience with cross browser and cross platform testing (with Browserstack and real Android\iOS devices);</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- experience with API testing (with Postman; SOAP UI);</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- experience with writing automated tests for Web UI and API (Python + Selenium + PyTest + Allure);</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- DevOps skills (work with GitLab, GitLab CI and Pages, Docker);</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- experience with Charles for mobile app testing;</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- work with healthcare websites and mobile application, admin panels, customer billing panels, CPI tools, online stores, news websites, text and video chats, social networks, etc.;</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- work with OpenCart and WordPress CMS;</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- work with Qualtrics and ZenDesk;</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- work with Mantis, OnTestPad, Asana, Bitrix24, Jira (+Confluence and Zephyr), Trello applications.</span>
      </p>
      <p class="c2">
         <span class="c1 c0">- work with Windows (XP, 7, 8.1, 10), Linux (Ubuntu 16.04/18.04/20.04), mac OS 10.14/10.15 as user;</span>
      </p>
      <div>
         <h3 class="c25 c16">
            <span class="c6 c8">My key skills</span>
         </h3>
         <table border="2" align="left" style="background-color:FCFFD4;" id="kryskills">
            <tr>
               <th> Skill </th>
               <th> Level </th>
            </tr>
            <tr>
               <th>Manual QA</th>
               <th bgcolor="D4DFFF" > <span> <progress min="0" max="3" value="3"></progress></span> </th>
            </tr>
            <tr>
               <th>Automation QA</th>
               <th bgcolor="D4DFFF" > <span> <progress min="0" max="3" value="2"></progress></span> </th>
            </tr>
            <tr>
               <th>Web and API technology</th>
               <th bgcolor="D4DFFF"> <span> <progress min="0" max="3" value="2"></progress></span>
               </th>
            <tr>
               <th>Python</th>
               <th bgcolor="D4DFFF"> <span> <progress min="0" max="3" value="2"></progress></span>
               </th>
            <tr>
               <th>DevOps</th>
               <th bgcolor="D4DFFF"> <span> <progress min="0" max="3" value="1"></progress></span>
               </th>
            <tr>
               <th>Linux</th>
               <th bgcolor="D4DFFF"> <span> <progress min="0" max="3" value="1"></progress></span>
               </th>
         </table>
      </div>
      <div>
         <p> Where: </p>
         <p> 0 - Trainee </p>
         <p> 1 - Junior </p>
         <p> 2 -  Middle </p>
         <p> 3 -  Senior </p>
      </div>
      <br>
      <h3 class="c20 c16" id="experience">
         <span>Work Experience</span>
      </h3>
      <h4 class="c9 c16" id="Inoxoft">
         <span>1. Inoxoft Lviv (</span>
         <span class="c5 c11">
         <a class="c3" href="https://inoxoft.com/">WebSite</a>
         </span>
         <span>) 21/12/2020 - 01/09/2022 </span>
      </h4>
      <p class="c15">
         <span class="c1 c0">Middle Automation QA
         <br>*Creating Test Plan, Coverage map, Test Cases, and Bug Reports (with Jira).
         <br>*Created, supporting, and improving Web Autotest Framework.
         <br>*Writing Autotests using Python+Selenium+Pytest for web UI.
         <br>*SetUp CI/CD flow for Automation Testing.
         <br>*Manage AQA Team (2 Team mates).
         </span>
      </p>
      <h4 class="c9 c16" id="globallogic">
         <span>2. GlobalLogic Lviv(</span>
         <span class="c5 c11">
         <a class="c3" href="https://www.globallogic.com/">WebSite</a>
         </span>
         <span class="c6 c13 c0">) 02/05/2019 - 18/12/2020 (1 year and 7 month)</span>
      </h4>
      <p class="c15">
         <span class="c1 c0">Middle Manual Test Engineer - web project 01/09/2019 - 18/12/2020
         <br>*Work with requirements and project documentation (SRS).
         <br>*Writing and updating test cases, create bug reports (with Jira).
         <br>*Web application and mobile application testing. Using as a tool Python scripts, SQL developer, Charles and Postman.
         <br>*Responsible for project activity
         </span>
      </p>
      <p class="c15">
         <span class="c1 c0">Middle Manual Test Engineer - desktop project 02/05/2019 - 01/09/2020
         <br>*Work with requirements and project documentation (SRS).
         <br>*Writing and updating test cases, create bug reports (with Jira).
         <br>*Desktop application testing. Used such testing techniques as functional, non-functional (Installation testing, Reliability testing), &#1089;hange related testing.
         <br>*Responsible for project activity
         </span>
      </p>
      <h4 class="c9 c16" id="geeksforless">
         <span>2. GeeksForLess (</span>
         <span class="c5 c11">
         <a class="c3" href="https://geeksforless.com/">WebSite</a>
         </span>
         <span class="c3">) 14/02/2019 - 26/04/2019 (2.5 month)</span>
      </h4>
      <p class="c15">
         <span>Technical Project Manager
         <br>*Receiving requirements from the customer. Setting tasks for developers, and development control. Reporting and transfer of the finished product to the customer.
         </span>
      </p>
      <h4 class="c9 c16" id="lexico">
         <span>3. Lexico Telecom LTD (</span>
         <span class="c5 c11">
         <a class="c3" href="https://www.lexico-voip.com/">WebSite</a>
         </span>
         <span class="c3">) 01/04/2018 - 14/02/2019 (10 month)</span>
      </h4>
      <p class="c15">
         <span class="c1 c0">Manual/Automation(Python) QA Specialist
         <br>*Creating project documentation logic scheme, user story, epics for tasks and documentation for users (HIW).Wrote test plans, checklist, bug reports (with Jira).
         <br>*Testing of web applications customer billing panel, admin panel, CPI products, etc). Used such testing techniques as functional, non-functional, hange related testing, cross browser testing, and security (access control)testing.
         <br>*Performed API tests with Postman.
         <br>*Wrote autotest (Python+Selenium+Pytest) for UI and API. Tests were performed locally or passed to the system administrator. Regression testing of 3 projects was automated.
         <br> *Analysis of customer feedback.</span>
      </p>
      <h4 class="c4" id="constructor">
         <span>4. Constructor of innovations (</span>
         <span class="c5 c11">
         <a class="c3" href="http://constructor-of-innovations.com"/>WebSite</a>
         </span>
         <span class="c3">) 09/01/2017 - 01/04/2018 (1 year and 3 month)</span>
      </h4>
      <p class="c15">
         <span class="c6 c0">Manual QA Specialist
         <br>*Creating project documentation (application map, logic description of application) and documentation for users (FAQ, HIW, about project, application requirements). Wrote test plans, checklists, bug reports (with Mantis, Asana), release reports.
         <br>*Testing of web applications (text and video chat, social network, online store, news website, admin panel, etc). Used such testing techniques as functional, non-functional, change related testing, cross browser and cross platform (with browserstack)testing, security (accesses control) testing.
         <br>*Performed API tests with Postman.
         <br>*Analysis of customer feedback.</span>
      </p>
      <h4 class="c4" id="megabyte">
         <span>5. MEGABYTE (</span>
         <span class="c5 c11">
         <a class="c3" href="http://mb1c.com/">WebSite</a>
         </span>
         <span class="3">) 04/2015 - 12/2016 (1 year and 8 month)</span>
      </h4>
      <p class="c7">
         <span class="c1 c0">1C Specialist (Tech Support) \ Internet Marketer + SMM</span>
      </p>
      <h4 class="c4" id="tehnocentr">
         <span>6. Tehnocentr Majak(</span>
         <span class="c5 c11">
         <a class="c3" href="https://zvit.net/">WebSite</a>
         </span>
         <span class="c3">) 11/2014 - 04/2015 (5 month)</span>
      </h4>
      <p class="c7">
         <span class="c1 c0">1C specialist (Tech Support)</span>
      </p>
      <h4 class="c4" id="gran">
         <span>7. Call center Gran (</span>
         <span class="c5 c11">
         <a class="c3" href="http://gran.team/">WebSite</a>
         </span>
         <span class="c3">) 02/2014 - 11/2014 (9 month)</span>
      </h4>
      <p class="c7">
         <span class="c1 c0">Support Line Specialist</span>
      </p>
      <h3 class="c20 c16">
         <span class="c6 c8" id="certificates">Certificates</span>
      </h3>
      <p class="c9">
         <span class="c0">- HTML (SOLOLearn - </span>
         <span class="c5 c0">
         <a class="c3" href="https://www.sololearn.com/Certificate/1014-5575532/pdf/">Certificate</a>
         </span>
         <span class="c1 c0">)</span>

      </p>
      <p class="c9">
         <span class="c0">- SQL (SOLOLearn - </span>
         <span class="c0 c5">
         <a class="c3" href="https://www.sololearn.com/Certificate/1060-5575532/pdf/">Certificate</a>
         </span>
         <span class="c1 c0">)</span>
      </p>
      <p class="c9">
         <span class="c0">- PYTHON 3 (SOLOLearn - </span>
         <span class="c5 c0">
         <a class="c3" href="https://www.sololearn.com/Certificate/1073-5575532/pdf/">Certificate</a>
         </span>
         <span class="c1 c0">)</span>
      </p>
      <p class="c9">
         <span class="c0">- Git (Udemy - </span>
         <span class="c5 c0">
         <a class="c3" href="https://www.udemy.com/certificate/UC-f74e0b11-482e-4a69-ba8d-d3d5c4fc1ba8/">Certificate</a>
         </span>
         <span class="c1 c0">)</span>
      </p>
      <p class="c9">
         <span class="c1 c0">listened the training ISTQB Certification by GlobalLogic - 11.2019</span>
      </p>
      <p class="c9">
         <span class="c1 c0">listened the training Python by GlobalLogic - 07.2020</span>
      </p>
      <h3 class="c20 c16">
         <span class="c6 c8" id="additional">Additional</span>
      </h3>
      <p class="c9">
         <span class="c1 c0">I acted as a lecturer at the events:</span>
      </p>
      <p class="c9">
         <span class="c0">- Lexico WorkShop (09.2018), with the </span>
         <span class="c5 c0">
         <a class="c3" href="https://docs.google.com/presentation/d/1mX7QbB4gDH6nviv3qMQwKJnZF9PAKZQveWj72lptrF8">presentation</a>
         </span>
         <span class="c0">.</span>
      </p>
      <p class="c9">
         <span class="c0">- Python MeetUp (10.2018), with the </span>
         <span class="c5 c0">
         <a class="c3" href="https://docs.google.com/presentation/d/19AfPHDHoNXje15Pp7QxEOHHfeNcvCODKEea8k6pBMY4">presentation</a>
         </span>
         <span class="c1 c0">.</span>
      </p>
      <p class="c9">
         <span class="c0">- Postman Tech Talk (07.2019), with the </span>
         <span class="c5 c0">
         <a class="c3" href="https://docs.google.com/presentation/d/1Q-w9Eu9wLsXuPz5OWfHoIuqNwYMkl8u6N9zC8rjgOv0">presentation</a>
         </span>
         <span class="c1 c0">.</span>
      </p>
      <p class="c9">
         <span class="c0">- PostMan WorkShop(07.2019), with the </span>
         <span class="c5 c0">
         <a class="c3" href="https://docs.google.com/presentation/d/1jLTqwyuXZLT8zXxSyRdM9V2e44oH1ureb5tBaIlud2Y">presentation</a>
         </span>
         <span class="c1 c0">.</span>
      </p>
      <p class="c9">
         <span class="c1 c0">- PostMan Training;(29.01.2020)</span>
      </p>
      <p class="c9">
         <span class="c0">Also Conducted classes with a group of 5 students as a mentor.</span>
      </p>
      <h3 class="c20 c16">
         <span class="c6 c8" id="languages">Languages</span>
      </h3>
      <p class="c9">
         <span class="c1 c0">Ukrainian, Russian - Fluent</span>
      </p>
      <p class="c9">
         <span class="c1 c0">English - Intermediate </span>
      </p>
      <h3 class="c16 c20">
         <span class="c6 c8" id="interests" >Interests</span>
      </h3>
      <p class="c9">
         <span class="c0">Lego, board games, video games, try to keep up with the latest technology news.</span>
      </p>
      <h3 class="c20 c16">
         <span class="c6 c8" id="references">References</span>
      </h3>
      <p class="c9">
         <span class="c0">Available upon request.</span>
      </p>
      <div>
      <h3 class="c16 c25" id="playground">
         <span class="c6 c8">Playground</span>
      </h3>
         <span class="c5 c0">
		 <a class="c3" href="https://www.w3schools.com/html/html_form_elements.asp">HTML Form Elements</a>
         </span>
		 <br>
         <span class="c5 c0">
		 <a class="c3" href="https://www.w3schools.com/html/html_form_input_types.asp">HTML Input Types</a>
	     </span>
	  </div>
      <div>
      <h3 class="c16 c25" id="video">
         <span class="c6 c8">Video</span>
      </h3>
         <iframe align="center" height="500" width="750" src="https://www.youtube.com/embed/lQQciBSiias" allowfullscreen frameborder="50"></iframe>
      </div>
	  <div>
      <p>Video courtesy of <a href="https://www.youtube.com/user/hillelitschool?app=desktop" target="_blank">Hillel school Youtube channel</a>.</p>
      </div>
   </body>
</html>`);
});

app.listen(3000, () => {
	console.log("Started api service");
});